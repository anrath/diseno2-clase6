namespace Ecommerce.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Buyer_UserId", c => c.Int());
            CreateIndex("dbo.Orders", "Buyer_UserId");
            AddForeignKey("dbo.Orders", "Buyer_UserId", "dbo.Users", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "Buyer_UserId", "dbo.Users");
            DropIndex("dbo.Orders", new[] { "Buyer_UserId" });
            DropColumn("dbo.Orders", "Buyer_UserId");
        }
    }
}
