(function () {
    'use strict';
    angular
        .module('App')
    .controller('Items.Controller', ItemsController);
    
    function ItemsController() {
        this.items = [
            {
                ID: 1,
                Name: 'Shirt',
                Price: 20,
                Description: 'This is the description for the shirt'
            },
            {
                ID: 2,
                Name: 'Pants',
                Price: 30,
                Description: 'This is the description for the pants'
            }
        ];

        this.formatPrice = function formatPrice(item) {
            return '$' + item.Price + '.00';
        }

        this.showDescription = function showDescription(item) {
            alert(item.Description);
        }
    }
    
})();