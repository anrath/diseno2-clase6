#Clase Angular 3

##Temas1. Two-Way binding2. Directivas comunes3. Directivas custom4. Routing##Getting other services

+ Agregar referencia del archivo js a la página.
+ Agregar la referencia al módulo en el código de la aplicacion AngularJS

```javascript
myApp = angular.module('myApp', ['module'])
```

####Ejemplo: incluir ng-messages

En nuestro archivo index.html agregamos el siguiente tag dentro del ```<head>```

```html
<script src="https://code.angularjs.org/1.4.6/angular-messages.js"></script>
```

Y en app.js, al inicializar nuestra app de AngularJs agregamos el módulo entre paréntesis.

```javascript
myApp = angular.module('myApp', ['ngMessages'])
```

De esta manera podemos utilizar los mensajes que nos provee el módulo, utilizando el siguiente ejemplo.

[Por más información sobre ngMessages](https://docs.angularjs.org/api/ngMessages/directive/ngMessages)

####Ejemplo para instalar Web Site

+ Habilitar cors

En la consola de paquetes Nuget```
Install-Package Microsoft.AspNet.WebApi.Cors
```

En el archivo WebApiConfig.cs

```C#
public static void Register(HttpConfiguration config){
        	var cors = new EnableCorsAttribute("http://localhost:2021", "*", "*");
 	config.EnableCors(cors);
 	
	config.Routes.MapHttpRoute(                name: "DefaultApi",                routeTemplate: "api/{controller}/{id}",                defaults: new { id = RouteParameter.Optional }
       ); }
```##Directives and two way binding###DirectiveUna directiva es una instrucción a AngularJS de que debe manipular una porción del DOM. Ya hemos utilizado algunas directivas antes, aunque sin darnos cuenta.Directivas que ya conocemos: ng-controller ng-app###ng-modelDirectiva que establece que lo que se determina queda atado a una variable del scope en particular.

```html
<input ng-model="handle"/>
<div class="alert alert-success">{{ handle }}</div>
```###Directivas comunes:+ [Ng-if](https://docs.angularjs.org/api/ng/directive/ngIf) - Genera una porción del DOM si se satisface la condición+ [Ng-show](https://docs.angularjs.org/api/ng/directive/ngShow) - Muestra una porción del DOM si se satisface la condición+ [Ng-hide](https://docs.angularjs.org/api/ng/directive/ngHide) - Oculta una porción del DOM si se satisface la condición+ [Ng-class](https://docs.angularjs.org/api/ng/directive/ngClass) - Asigna clases css dependiendo de condiciones+ [Ng-repeate](https://docs.angularjs.org/api/ng/directive/ngRepeat) - Permite repetir un template+ [Ng-click](https://docs.angularjs.org/api/ng/directive/ngClick) - Captura el evento click sobre un elemento+ [Ng-cloak](https://docs.angularjs.org/api/ng/directive/ngCloak) - Permite ocultar la interpolación de texto de AngularJS.Para encontrar más directivas, sigan este [link](https://angularjs.org/api). Si navegan a la sección de directives, podrán encontrar todas las que se encuentran disponibles. Además, pueden usar directivas no creadas por angular.##SPA's with AngularJS###Multiple Controllers and multiple viewsEjemplo de multiples controllers y multiples divs con distintos scopes.###HashChange```javascript

window.addEventListener('hashchange', function(){
	console.log('Hash changed!: ' + window.location.hash);})
```
Si vamos  al navegador sobre nuestra app y sobre el final de la url, agregamos '#nuevohash', y le damos enter, observamos que sale logueado por consola. Lo mismo pasa si usamos #/folder/nuevo.


###$location service

```javascript
$log.log($location.path)
```

##ng-route

Adding ng-route

```javascript
var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(function($routeProvider) {
	$routeProvider
	
	.when('/', {
		templateUrl: 'pages/main.html', 
		//el html que va a usar
		controller: 'mainController' 
		//el controller que va a manejar lo que allí suceda	})	.when('/users', {
		templateUrl: 'pages/users.html', 
		//el html que va a usar
		controller: 'userController' 
		//el controller que va a manejar lo que allí suceda	})})
```

##ng-view
Esta directiva es la que le indica a AngularJS donde debe insertar el contenido de la vista seleccionada.
Si agregamos esta directiva, el framework incluirá en este div lo que determine el routing.

Sustituimos entonces el siguiente div

```
<div ng-controller="....">
...
</div>
```
Por el tag HTML de ng-view. AngularJS va a sustituir ng-view por el template HTML correspondiente.

```<ng-view>```


Queda entonces un código HTML más limpio y mantenible.



##Directivas Custom

###Cuándo usarlas?
Sirven cuando las directivas que están disponibles no alcanzan para lo que queremos hacer, cuando queremos encapsular complejidad en código en lugar de en HTML o cuando queremos crear una unidad de funcionalidad auto contenida que podamos reutilizar.


Crearemos una directiva que genere un elemento ```ul``` por cada elemento de un array

- ###Definimos La directiva en app.js

```javascript
angular.module("App")
	.directive("unorderedList", function(){
		return function(scope, element, attrs){
			//Y aquí pondremos el código		}	})
	.controller("defaultCtrl", function($scope) {
		$scope.products = [
			{ name: "Manzana", category: "Fruta", price: 1.20 },
			{ name: "Banana", category: "Fruta", price: 2.20 },
			{ name: "Anana", category: "Fruta", price: 4.00 }
		];	})
```

Agregamos además un controller que nos permita probar la nueva directiva.
El primer argumento que le paso al método directiva setea el nombre de la nueva directiva. Hay que tener cuidado con el casing, ya que AngularJs es muy particular con ellas.

Luego agregamos el siguiente código en nuestro index.html

```javascript
<body ng-controller="defaultCtrl">    <div class="panel panel-default">        <div class="panel-heading">            <h3>Products</h3>        </div>        <div class="panel-body">            <div unordered-list="products"></div>        </div>    </div></body>
```

+ ####Implementamos la función enlace
La función que acabamos de agregar a la directiva es conocida como función enlace, ya que permite enlazar el HTML del documento con los datos en el scope. Esta función es llamada cuando AngularJs inicializa cada instancia de la directiva y recibe tres argumentos: scope, elemento HTML al que se aplica la directiva, y los atributos de dicho elemento HTML. La convención dicta que se deben nombrar como se muestra arriba.

	- ####Obtener datos del scope
Primero debemos obtener los datos que voy a mostrar desde el scope. A diferencia de los controladores, las directivas no declaran dependencia al servicio $scope. Se les pasa el scope que el controlador que soporta la vista crea. Esto es lo que permite que una misma directiva se aplique múltiples veces en una misma aplicación AngularJS.


```javascript
angular.module("App")
	.directive("unorderedList", function(){
		return function(scope, element, attrs){
			var data = scope[attrs["unorderedList"]];
			if(angular.isArray(data)){
				for(var i = 0; i< data.length; i++){
					console.log("Item: " + data[i].name);				}			}		}	})
```

La línea que me permite obtener los atributos del elemento es la siguiente.

```var data = scope[attrs["unorderedList"]];```

una vez que tengo los datos, utilizo el método angular.isArray para asegurarme que estoy trabajando con un array, y que puedo iterar sobre el.
Si cargamos la página, veremos el siguiente resultado en la consola:

```javascript
Item: Manzana
Item: Banana
Item: Anana
```


+ ####Generar elementos HTML
El siguiente paso es generar los elementos que necesitamos para representar a los objetos.

```javascript
angular.module("App")
	.directive("unorderedList", function(){
		return function(scope, element, attrs){
			var data = scope[attrs["unorderedList"]];
			if(angular.isArray(data)){
				var listElem = angular.element("<ul>");               			 element.append(listElem);                			for (var i = 0; i < data.length; i++) {                   			listElem.append(angular.element('<li>')
                   					.text(data[i].name));               			}			}		}	})
```

Llamando al método angular.element() creo el nuevo elemento y uso append para agregar el nuevo elemento al documento

	
- ###Romper la dependencia con el objeto data
	+ ####Agregar un atributo de soporte
	+ ####Evaluando expresiones
- ###Manejando cambios en los datos
	+ ####Agregar el watcher
	+ ####Arreglar el problema léxico del Scope
	